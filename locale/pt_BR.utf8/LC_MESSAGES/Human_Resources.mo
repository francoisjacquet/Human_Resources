��          �      l      �     �     �     �          "  
   @  	   K     U  	   ^     h     �      �     �     �     �     �     �     �             �       �     �                ,  	   M  	   W     a     h  "   p  !   �  &   �     �     �     �  
   �     
               )                                                  	   
                                             Certification Certifications Completed on Elementary proficiency Full professional proficiency Granted on Institute Language Languages Limited working proficiency Native or bilingual proficiency Professional working proficiency Qualification Qualifications Reading Skill Skills Speaking Valid through Writing Project-Id-Version: Human Resources module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:07+0200
Last-Translator: Emerson Barros
Language-Team: RosarioSIS <info@rosariosis.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Certificação Certificações Concluído em Proficiência elementar Plena proficiência profissional Obtido em Instituto Idioma Idiomas Proficiência de trabalho limitada Proficiência nativa ou bilíngue Proficiência de trabalho profissional Qualificação Qualificações Ler Habilidade Habilidades Falar Válido até Escrever 