��          �      l      �     �     �     �          "  
   @  	   K     U  	   ^     h     �      �     �     �     �     �     �     �             �               	        '  $   A     f     s     |     �  $   �  )   �  &   �       	   
               #     0     7     G                                                  	   
                                             Certification Certifications Completed on Elementary proficiency Full professional proficiency Granted on Institute Language Languages Limited working proficiency Native or bilingual proficiency Professional working proficiency Qualification Qualifications Reading Skill Skills Speaking Valid through Writing Project-Id-Version: Human Resources module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:07+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Certification Certifications Obtenu le Compétence élémentaire Compétence professionnelle avancée Accordée le Institut Langue Langues Compétence professionnelle limitée Compétence bilingue ou langue maternelle Compétence professionnelle générale Diplôme Diplômes Lu Compétence Compétences Parlé Valide jusqu'à Écrit 